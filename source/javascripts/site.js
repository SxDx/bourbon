import Unsplash, { toJson } from "unsplash-js"
const axios = require('axios')

const collections = [2180676]

const unsplash = new Unsplash({
  accessKey: unsplashAppId,
  secret: unsplashAppSecret
})

$(document).on("keydown", (event) => {
  if (event.key === "?") {
    showHelp()
  } else if (event.key === "Escape") {
    hideHelp();
  }
})

$(document).ready(function () {
  // Load inital Background
  loadCurrentBackground().then(bg => {
    setBackground(bg)
  })
  
  $("#reload").on("click", function (_event) {
    getRandomBackground().then(bg => {
      setBackground(bg)
      saveCurrentBackground(bg)
    })
  })

  $("li").hover(
    function (_event) {
      let $target = $(this)
      let newColor = $target.find("a").data("hover-color")
      $target.find("span.inline-block").css("background-color", newColor)
    },
    function (_event) {
      let $target = $(this)
      $target.find("span.inline-block").css("background-color", "#22292f")
    }
  )
})

// Background
function getRandomBackground() {
  return unsplash.photos.getRandomPhoto({ collections: collections })
    .then(toJson)
}

function saveCurrentBackground(background) {
  axios.put(storeUrl, background)
}

function loadCurrentBackground() {
  return axios.get(storeUrl + "/latest").then(response => {
    return response.data
  })
}

function setBackground(background) {
  console.log(background)
  let url = background.urls.full
  document.body.style.backgroundImage = `url('${url}')`
  setMetaData(background)
}

function setMetaData(background) {
  let parts = []
  if(background.location) {
    parts.push(background.location.title)
  }
  parts.push(`${background.user.first_name} ${background.user.last_name}`)

  parts = parts.filter(function (el) {
    return el != null && el.length > 0;
  });

  $("#meta").text(parts.join(", ")).attr("href", background.links.html)
}

function showHelp() {
  $("#help").addClass("opacity-100 visible").removeClass("opacity-0 invisible")
}

function hideHelp() {
  $("#help").addClass("opacity-0").removeClass("visible").delay(600).addClass("invisible").removeClass("visible")
}