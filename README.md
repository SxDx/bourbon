# Development

## Requirements
* Ruby 2.6+
* Yarn
* jsonbin.io account
* unsplash dev account

## Dependencies
* ```bundle install```
* ```yarn install```

## Start dev server
* ```cp .env.example .env```
* Fill out .env with your variables
* Edit ```data/bookmarks.yml```
* ```bundle exec middleman serve```

## Deployment
* Gitlab Repo: Settings -> CI /CD -> Variables
* Enter .env variable keys and values